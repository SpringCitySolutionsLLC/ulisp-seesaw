; i2c-address.lisp.example
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; A mutable global variable defining the 
; I2C addrs of the seesaw.
; Its possible to have multiple chips on a bus
; so it has to be mutable.

; Copy this file to i2c-address.lisp, 
; which has a .gitignore line,
; then edit it to match your specific hardware 
; as mentioned in the comments below.

; default I2C addrs for seesaw breakout boards aka ada#3657
; (defvar *seesaw:i2c-address* #x49)

; This note applies to I2C addresses on the adafruit breakout board.
; If address select pin 0 (PA16) is tied to ground on boot,
; the I2C address is incremented by 1.
; If address select pin 1 (PA17) is pulled low,
; the I2C address is incremented by 2.
; If both address select pins are pulled low,
; the I2C address is incremented by 3.

; So for example, if you bought an Adafruit breakout
; board and shorted the PA17 address pin to ground, the
; board would be on addr #x4b

; default I2C addrs for Adafruit TFT boards aka ada#802
; (defvar *seesaw:i2c-address* #x2e)

; end of i2c-address.lisp.example file
