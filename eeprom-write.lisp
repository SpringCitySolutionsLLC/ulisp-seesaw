; eeprom-write.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; Writes a decimal byte to an EEProm Address

; Example:
; Store decimal 99 in eeprom address #x00:
; (seesaw:eeprom-write #x00 99)
; Read decimal 99 from eeprom address #x00:
; (seesaw:eeprom-read #x00)
; Store decimal 11 in eeprom address #x00:
; (seesaw:eeprom-write #x00 11)
; Read decimal 11 from eeprom address #x00:
; (seesaw:eeprom-read #x00)

; TODO: eeprom address #x3f is supposed to store the I2C device
; address.  It does not seem to do so.
; I would not touch that address; might brick the device?

; My tft display seesaw does not have eeprom compiled in.

; This works fine on my breakout board.

(defun seesaw:eeprom-write (address data-byte)
  (with-i2c (str *seesaw:i2c-address*)
            (write-byte #x0d str)
            (write-byte address str)
            (write-byte data-byte str)
            ))

; end of eeprom-write.lisp file
