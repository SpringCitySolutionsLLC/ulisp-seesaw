; pwm.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Set PWM output level on a pin

; pin name is quoted standard hardware name example 'PA04

; percent is either a hexadecimal byte percentage, example #x7F would be half,
; or there are convenience quotes where it interprets 'on 'full 'half 'off

; For example,
; Adafruit 1.8 TFTv2 board backlight is on pin 
; PA04 so either:
; (seesaw:pwm 'PA04 #x7f)
; (seesaw:pwm 'PA04 'half)
; would set the backlight to half brightness

(defun seesaw:pwm (pin percent)
  (cond 
    ((eq pin 'PA04) (setq pin #x00))
    ((eq pin 'PA05) (setq pin #x01))
    ((eq pin 'PA06) (setq pin #x02))
    ((eq pin 'PA07) (setq pin #x03))
    )
  (cond 
    ((eq percent 'on) (setq percent #xFF))
    ((eq percent 'full) (setq percent #xFF))
    ((eq percent 'half) (setq percent #x7F))
    ((eq percent 'off) (setq percent #x00))
    )
  (with-i2c (str *seesaw:i2c-address*) 
            (write-byte #x08 str) 
            (write-byte #x01 str) 
            (write-byte pin str)
            (write-byte percent str))
  t
  )

; end of pwm.lisp file
