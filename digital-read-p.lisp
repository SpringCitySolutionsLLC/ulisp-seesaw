; digital-read-p.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Read a GPIO digital input port on the seesaw chip 
; returning T for high logical one, and NIL for low logical zero.

; For example, adafruit 1.8 TFTv2 board has a UI 
; button "A" on pin PA10 that is active low.
; Assuming the pin has previously been set to input-pullup by
; (seesaw:digital-direction 'PA10 'input-pull-up)
; then running 
; (seesaw:digital-read-p 'PA10)
; would return t when button "A" is not pressed, 
; or returns nil when button "A" is pressed

; TODO: input errors
; Sometimes reads return all #xFF
; for the UI on the TFTv2 that is not a major
; issue, merely means one missed poll because
; the buttons are all active low.
; None the less for generic seesaw use it should
; be fixed.  Perhaps keep on polling until the 
; bytes returned are not all #xFF.
; Adding delays to the I2C does not appear to help.

(defun seesaw:digital-read-p (pin)
  (let (
        
        (gpiobyte1 #x00)
        (gpiobyte2 #x00)
        (gpiobyte3 #x00)
        (gpiobyte4 #x00))
    
    (with-i2c (str *seesaw:i2c-address*)
              (write-byte #x01 str)
              (write-byte #x04 str)
              (restart-i2c str 4) 
              (setq gpiobyte1 (read-byte str))
              (setq gpiobyte2 (read-byte str))
              (setq gpiobyte3 (read-byte str))
              (setq gpiobyte4 (read-byte str)) 
              )
    
    (cond 
      ((eq pin 'PA00) (logbitp 0 gpiobyte4))
      ((eq pin 'PA01) (logbitp 1 gpiobyte4))
      ((eq pin 'PA02) (logbitp 2 gpiobyte4))
      ((eq pin 'PA03) (logbitp 3 gpiobyte4))
      ((eq pin 'PA04) (logbitp 4 gpiobyte4))
      ((eq pin 'PA05) (logbitp 5 gpiobyte4))
      ((eq pin 'PA06) (logbitp 6 gpiobyte4))
      ((eq pin 'PA07) (logbitp 7 gpiobyte4))
      ((eq pin 'PA08) (logbitp 0 gpiobyte3))
      ((eq pin 'PA09) (logbitp 1 gpiobyte3))
      ((eq pin 'PA10) (logbitp 2 gpiobyte3))
      ((eq pin 'PA11) (logbitp 3 gpiobyte3))
      ((eq pin 'PA12) (logbitp 4 gpiobyte3))
      ((eq pin 'PA13) (logbitp 5 gpiobyte3))
      ((eq pin 'PA14) (logbitp 6 gpiobyte3))
      )
    
    )  
  )

; end of digital-read-p.lisp file
