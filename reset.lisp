; reset.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Software reset the seesaw chip
; Sets PWM outputs to zero, etc.

; Software reset the seesaw
(defun seesaw:reset nil
  (with-i2c (str *seesaw:i2c-address*) 
            (write-byte #x00 str) 
            (write-byte #x7f str) 
            (write-byte #xff str))
  (delay 500)
  t
  )

; end of reset.lisp file
