; options-read.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; Read the options configured into the seesaw chip
; Returns a list of compilation options, or nil if no options
; are compiled in, or more likely the chip is non-responsive.

; Typical result from a seesaw breakout board
; (seesaw:options-read)
; (gpio sercom adc dac dap touch)

; Typical result from a tft-v2 LCD shield
; (seesaw:options-read)
; (gpio sercom adc)

(defun seesaw:options-read nil

  (let ((rawresult #x0000))

    (with-i2c (str *seesaw:i2c-address*)
              (write-byte #x00 str)
              (write-byte #x03 str)
              (restart-i2c str 4)
              (setq rawresult (+ (* 16777216 (read-byte str))
                                 (* 65536 (read-byte str))
                                 (* 256 (read-byte str))
                                 (read-byte str)
                                 ))
              )

    (append
      (if (logbitp 0 rawresult) (list 'gpio))
      (if (logbitp 1 rawresult) (list 'sercom))
      (if (logbitp 7 rawresult) (list 'timer))
      (if (logbitp 8 rawresult) (list 'adc))
      (if (logbitp 9 rawresult) (list 'dac))
      (if (logbitp 10 rawresult) (list 'interrupt))
      (if (logbitp 11 rawresult) (list 'dap))
      (if (logbitp 12 rawresult) (list 'eeprom))
      (if (logbitp 13 rawresult) (list 'neopixel))
      (if (logbitp 14 rawresult) (list 'touch))
      (if (logbitp 15 rawresult) (list 'keypad))
      (if (logbitp 16 rawresult) (list 'encoder))
      )
    )
  )

; end of options-read.lisp file
