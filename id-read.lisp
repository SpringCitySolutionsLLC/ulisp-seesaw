; id-read.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; Read the hardware id configured into the seesaw chip

; Typical result for any seesaw chip
; (seesaw:id-read)
; 85

; ID should be 0x55 hexadecimal, and the REPL will return that as 85 decimal.

(defun seesaw:id-read nil
  (with-i2c (str *seesaw:i2c-address*)
            (write-byte #x00 str)
            (write-byte #x01 str)
            (restart-i2c str 1)
            (read-byte str) ))

; end of id-read.lisp file
