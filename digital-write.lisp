; digital-write.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Write to a GPIO digital output port on the seesaw chip 

; pin name is quoted standard hardware name example 'PA27

; state synonyms for 'set include 'on 'h 'high
; state synonyms for 'clr include 'off 'l 'low

; TODO: try this
; For example, adafruit seesaw breakout board has a LED 
; on pin PA27 that is active high.
; Assuming the pin has previously been set to output by
; (seesaw:digital-direction 'PA27 'output)
; then running 
; (seesaw:digital-write 'PA27 'on)
; would light the LED

(defun seesaw:digital-write (pin state)
  (let (
        
        (gpiobyte1 #x00)
        (gpiobyte2 #x00)
        (gpiobyte3 #x00)
        (gpiobyte4 #x00))
    
    (cond 
      ((eq pin 'PA00) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x01))
      ((eq pin 'PA01) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x02))
      ((eq pin 'PA02) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x04))
      ((eq pin 'PA03) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x08))
      ((eq pin 'PA04) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x10))
      ((eq pin 'PA05) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x20))
      ((eq pin 'PA06) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x40))
      ((eq pin 'PA07) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x80))
      ((eq pin 'PA08) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x01)(setq gpiobyte4 #x00))
      ((eq pin 'PA09) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x02)(setq gpiobyte4 #x00))
      ((eq pin 'PA10) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x04)(setq gpiobyte4 #x00))
      ((eq pin 'PA11) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x08)(setq gpiobyte4 #x00))
      ((eq pin 'PA12) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x10)(setq gpiobyte4 #x00))
      ((eq pin 'PA13) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x20)(setq gpiobyte4 #x00))
      ((eq pin 'PA14) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x40)(setq gpiobyte4 #x00))
      ((eq pin 'PA15) (setq gpiobyte1 #x00)(setq gpiobyte2 #x00)(setq gpiobyte3 #x80)(setq gpiobyte4 #x00))
      ((eq pin 'PA16) (setq gpiobyte1 #x00)(setq gpiobyte2 #x01)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA17) (setq gpiobyte1 #x00)(setq gpiobyte2 #x02)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA18) (setq gpiobyte1 #x00)(setq gpiobyte2 #x04)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA19) (setq gpiobyte1 #x00)(setq gpiobyte2 #x08)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA20) (setq gpiobyte1 #x00)(setq gpiobyte2 #x10)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA21) (setq gpiobyte1 #x00)(setq gpiobyte2 #x20)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA22) (setq gpiobyte1 #x00)(setq gpiobyte2 #x40)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA23) (setq gpiobyte1 #x00)(setq gpiobyte2 #x80)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA24) (setq gpiobyte1 #x01)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA25) (setq gpiobyte1 #x02)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA26) (setq gpiobyte1 #x04)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      ((eq pin 'PA27) (setq gpiobyte1 #x08)(setq gpiobyte2 #x00)(setq gpiobyte3 #x00)(setq gpiobyte4 #x00))
      )
    
    (cond 
      ((eq state 'on) (setq state 'set))
      ((eq state 'h) (setq state 'set))
      ((eq state 'high) (setq state 'set))
      ((eq state 'off) (setq state 'clr))
      ((eq state 'l) (setq state 'clr))
      ((eq state 'low) (setq state 'clr))
      )
    
    (cond
      
      ((eq state 'set)
       (with-i2c (str *seesaw:i2c-address*)
                 (write-byte #x01 str)
                 (write-byte #x05 str)
                 (write-byte gpiobyte1 str)
                 (write-byte gpiobyte2 str)
                 (write-byte gpiobyte3 str)
                 (write-byte gpiobyte4 str)
                 ))
      
      ((eq state 'clr)
       (with-i2c (str *seesaw:i2c-address*)
                 (write-byte #x01 str)
                 (write-byte #x06 str)
                 (write-byte gpiobyte1 str)
                 (write-byte gpiobyte2 str)
                 (write-byte gpiobyte3 str)
                 (write-byte gpiobyte4 str)
                 )))
    t
    ))
  
; end of digital-read-p.lisp file
