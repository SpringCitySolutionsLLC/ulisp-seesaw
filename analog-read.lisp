; analog-read.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Read a 16 bit analog voltage on a specific pin

; Example reading the voltage of pin PA03
; (seesaw:analog-read 'PA03)

; If I read the output of GetOption correctly, my
; tft display seesaw does not have analog compiled in.

; TODO: Test this later with a plain breakout board.
; does not seem to be compiled into the TFTv2 board seesaw.

; TODO: optimize the read delay.  Technically 0.5 ms is long enough.  2 should be fine.

(defun seesaw:analog-read (pin)
  
  (let (
        (port #x07)
        (result-msb #x00)
        (result-lsb #x00)
        )
    
    (cond 
      ((eq pin 'PA02) (setq port #x07))
      ((eq pin 'PA03) (setq port #x08))
      ((eq pin 'PA04) (setq port #x09))
      ((eq pin 'PA05) (setq port #x0A))
      ) 
    
    (with-i2c (str *seesaw:i2c-address*)
              (write-byte #x09 str)
              (write-byte port str)
              (restart-i2c str 2)
              (delay 2)
              (setq result-msb (read-byte str))
              (setq result-lsb (read-byte str))
              )
    
    (+ (* 256 result-msb) result-lsb)
    
    ))

; end of analog-read.lisp file
