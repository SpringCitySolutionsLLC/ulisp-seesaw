; version-read.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; Returns a list of the firmware decimal date code
; and firmware decimal software version

; Typical result from a #3657 seesaw breakout board
; (seesaw:version-read)
; (3657 9047)

; Typical result from a #802 tft-v2 LCD shield
; (seesaw:version-read)
; (0 9471)

(defun seesaw:version-read nil
  (with-i2c (str *seesaw:i2c-address*)
            (write-byte #x00 str)
            (write-byte #x02 str)
            (restart-i2c str 4)
            (append

              (list (+ (* 256 (read-byte str))
                       (read-byte str)))

              (list (+ (* 256 (read-byte str))
                       (read-byte str)))

              )
            ))

; end of version-read.lisp file
