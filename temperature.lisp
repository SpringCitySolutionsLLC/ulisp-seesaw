; temperature.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; Read the temperature of the seesaw chip

; Example:
; (seesaw:temperature)
; should return the temperature in C or F or something of the seesaw chip.

; TODO: This option does not seem to be compiled into the
; Adafruit(tm) TFTv2 boards seesaw by default.
; I will try it later on a dedicated seesaw dev board.

; If I run this on the breakout board, I'll get one response 19203 decimal
; and then it responds all #xFF until I run another command.

; Maybe it needs a custom firmware of some sort.

(defun seesaw:temperature nil

  (let ((rawresult #x0000))

    (with-i2c (str *seesaw:i2c-address*)
              (write-byte #x00 str)
              (write-byte #x04 str)
              (restart-i2c str 4)
              (setq rawresult (+ (* 16777216 (read-byte str))
                                 (* 65536 (read-byte str))
                                 (* 256 (read-byte str))
                                 (* 1 (read-byte str))
                                 ))

              )

    )
  )


; the python library does something along these lines to
; post process the data bytes:
;   buf[0] = buf[0] & 0x3F
;   ret = struct.unpack(">I", buf)[0]
;   return 0.00001525878 * ret

; end of temperature.lisp file
